import pandas as pd
import numpy as np
from datetime import datetime as dt
from datetime import timedelta as td

@np.vectorize
def clean_id(id):
    id = id.strip()
    if id == '':
        return 'INVALID'
    for ch in id:
        if not ch.isdigit():
            return 'INVALID'
    return id.zfill(8)[-8:]

@np.vectorize
def identify_opr(field, cp, fo):
    field = field.strip()
    if field == 'CAPT':
        return cp
    if field == 'F.O.':
        return fo

def set_key(df):
    df['key'] = df.cif + ' ' + df.date.dt.strftime('%Y-%m-%d')

def get_last_3_opr_date(df):
    df = df.sort_values('date')
    df = df.groupby('cif').tail(3).groupby('cif')
    df = df.date.aggregate(['count', 'min']).reset_index()
    df.columns = ['cif','count', 'date']
    df = df[df['count'] >= 3]
    return df[['cif', 'date']]

@np.vectorize
def concat_acars_datetime(date, time):
    time = str(time)[-4:]
    hour = time[-4:-2]
    minute = time[-2:]
    
    h = td(hours=int(hour))
    m = td(minutes=int(minute))
    
    return pd.to_datetime(date) + pd.to_timedelta(h + m)

def __get_flight(flt_id, time, fld):
    
    if flt_id=='':
        return np.nan
    
    df = fld[fld.flt==str(flt_id)]
    df = df[df.arr > (time - pd.to_timedelta(td(hours=18)))]
    df = df[df.arr < (time + pd.to_timedelta(td(minutes=10)))]
    if df.shape[0] == 1:
        return float(df.index[0])
    
    elif df.shape[0] == 0:
        return np.nan
    
    else:
        return (df.arr - pd.to_datetime(time)).abs().idxmin()

get_flight = np.vectorize(__get_flight, excluded=[2])


@np.vectorize
def apply_rex(rex_45, rex_90, nl_exp):
    date = min(rex_45, rex_90)
    date = max(date, nl_exp)
    return date


def cross_records(df, reference, key, reference_key, indicator):
    
    reference = reference[reference_key]
    reference.columns = [f'{col}_ref' for col in reference.columns]
    
    df = df.merge(
        reference,
        left_on=key,
        right_on=list(reference.columns),
        how='left',
        indicator=indicator
    )
    
    df = df.drop([c for c in df.columns if c.endswith('ref')], axis=1)
    
    df[indicator] = np.where(df[indicator] == 'both', True, False)
    
    return df
